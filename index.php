<?php

$servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadora";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed!");
    }

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {
  font-family: "Lato", sans-serif;
  background-color: #bebebe
}

html, ul, li, img, div{
  margin:0; padding:0;
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #249462;
  /* rgb(5, 118, 133); */
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  /*color: #f1f1f1;*/
  color: #000000;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 25px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

/*navbar fixa*/
.navbar {
  overflow: hidden;
  background-color: #333;
  box-shadow: 10px 10px 5px grey;
  height: 48px;
}

.navbar a {
  float: left;
  font-size: 17px;
  color: white;
  text-align: center;
  padding: 15px 16px;
  text-decoration: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 6px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: #249462;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}


/* user area */
.userdropdown {
  float: left;
  overflow: hidden;
}

.userdropdown .dropbtn { 
  border: none;
  outline: none;
  color: white;
  padding: 6px;
  background-color: inherit;
  margin: 0;
}

.navbar a:hover, .userdropdown:hover .dropbtn {
  background-color: #249462;
}

.userdropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.userdropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.userdropdown-content a:hover {
  background-color: #ddd;
}

.userdropdown:hover .dropdown-content {
  display: block;
}
/* 
.searchButton a:hover{
  background-color: #249462"
} */


/* .horizontal-scroll-wrapper{
  position:absolute;
  display:block;
  left:20%; /* centralizando essa coisa 
  width:200px; /* tamanho dessa coisa em altura 
  max-height:400px; /*tamanho dessa coisa em largura
  margin:0;
  overflow-y:auto;
  overflow-x:hidden;
  transform:rotate(-90deg) translateY(-80px);
  transform-origin:right top;
}
.horizontal-scroll-wrapper > div{
  transform:rotate(90deg);
  transform-origin: right top;
}

.rectangles{
  top:70px;
  padding-bottom:100px; /* estranhamente isso tbm muda o tamanho do negocio *
}
.rectangles > .mySlide{
  width:140px; /* quanto maior, + perto da scroll *
  height:505px; /* aqui é o distanciamento entre as imagens *
  margin:0px 40px; /* isso interfere diretamente na centralização das imagens *
  padding-left:90px; /* isso aqui tbm interfere a centralização das imagens *
  transform:rotate(90deg) translateY(80px);
  transform-origin: right top;
} */


/* parte de CSS do slideshow automatizado */
.auto-slider img{
  display:block;
  width : 505px;
  height : 190px;
}
.auto-slider{
  outline:1px solid black;
  width:505px;
  height:190px;
  margin-top : 5%;
  overflow-x:hidden;
}

.auto-slider__content{
  list-style:none;
  display:flex;
/*   animation-name: slide;
  animation-duration:10s;
  animation-iteration-count: infinite;
  animation-timing-function:ease-in-out; */
  
  animation: slide 10s ease-in-out infinite;
/*   animation-direction: alternate-reverse; */
}
.auto-slider__content:hover{
  animation-play-state:paused;    
}

@keyframes slide{
  10%{
    transform:translateX(0);
  }
  15%, 30%{
    transform:translateX(-505px);
  }
  35%, 50%{
    transform:translateX(-1010px);
  }
  55%, 70%{
    transform:translateX(-1515px);
  }
  75%, 90%{
    transform:translateX(0);
  }
}






.corpo{
  display:flex;
}

.logo{
  height: 256px;
  width:  256px;
  padding-top: 16px;
  padding-right: 13%;
}

input[type=search]
{
  color: black;  
	text-align: left;
	cursor: pointer;
	display: block; 
	width: 154px;
	letter-spacing: 2px;
    height:23px;
    font-size: 12px;
}

#nct:hover{
  background-color : #333;
}

/* https://fonts.google.com/specimen/Source+Sans+Pro
https://fonts.google.com/specimen/Dosis
https://fonts.google.com/specimen/Exo */


</style>
</head>
<body>
<div id="navbar" class="navbar">
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="#">About</a>
        <a href="#">Services</a>
        <a href="#">Clients</a>
        <a href="#">Contact</a>
    </div>
    <a><span style="text-align:left;font-size:16px;cursor:pointer;" onclick="openNav()">&#9776; Menu</span></a>
    
    <a style="background-color:#249462;" href="index.php">Home</a>
    <a href="https://news.google.com/?hl=pt-BR&gl=BR&ceid=BR:pt-419" target="_blank">News</a>
    <div class="dropdown">
      <button class="dropbtn">Locar 
        <i class="fa fa-caret-down"></i>
      </button>
      <div class="dropdown-content">
        <a href="locarCarro.php">Carro</a>
        <!-- <a href="locarMoto.php">Moto</a> -->
      </div>
    </div> 
    <a href="loginScreen.php">Login</a>
    <a href="signupScreen.php">Cadastrar</a>
    <a id="nct"><img src="res/images/nct-logo.png" style="margin-left : 100px; width : 42px; height : 30px;"></a>
    <!-- <a id="searchText"><input type="search" results="5" autosave=some_unique_value placeholder=Buscar.. onfocus="this.placeholder=''"></a>
    <a onclick=""href='http://localhost/locadora/locarCarro.php' style="padding: 16px 10px; height:16px; width:16px;">
      <i class="fa fa-search">
        <button class="searchButton" style="border:none;visibility: hidden">
        </button>
      </i>
    </a> -->
    <?php
        $sqlUser = "SELECT email FROM usuario WHERE logado=1";
        $conectado = mysqli_query($conn,$sqlUser);
        //Comando para executar o SQL
        $email = mysqli_fetch_row($conectado);
        //Comando que pega as linhas do resultado
        $email = $email[0];
        //Pega a coluna 0 da variavel email e ja passa pra ela msm
        if(mysqli_num_rows($conectado) > 0) //Se o numero de linhas for > q 0, ele executa esse HTML
        { 
         echo "<div class='userdropdown' style='visibility: visible; position : flex; float : right'>";
         echo   "<button class='dropbtn'> <img src='res/images/user-icon1.png' height='30' width='30'>"; 
         echo       "<i class='fa fa-caret-down'></i>";
         echo   "</button>";
         echo   "<div class='dropdown-content'>";

         echo       "<a href='http://localhost/locadora/editprofileScreen.php'>Edit Profile</a>";
         echo       "<a href='http://localhost/locadora/devolucaoScreen.php'>Locações Pendentes</a>";
         echo       "<a href='http://localhost/locadora/logout.php'>Log Out</a>";
         echo   "</div>";
         echo   "<a  style='float : right'>Olá ". $email ."</a>";
         echo "</div>";
        }
        else //Senão, mantem do jeito que ta
        {
          echo "<div class='userdropdown' style='visibility: hidden; position : absolute'>";
          echo   "<button class='dropbtn'> <img src='res/images/user-icon1.png' height='30' width='30'>"; 
          echo       "<i class='fa fa-caret-down'></i>";
          echo   "</button>";
          echo   "<div class='dropdown-content'>";
          echo       "<a href='#'>Edit Profile</a>";
          echo       "<a href='http://localhost/locadora/devolucao.php'>See History</a>";
          echo       "<a href='http://localhost/locadora/logout.php'>Log Out</a>";
          echo   "</div>";
          echo "</div>";
        }
    ?>
     
</div>
<!-- https://www.youtube.com/watch?v=SYw3TTLtZLc automatic css slideshow -->
<div class="corpo">
  <img class="logo" src="res/images/neocar.png">
  <!--Parte de slide das imagens manual-->
  <!-- <div class="horizontal-scroll-wrapper  rectangles">
    <div class="mySlide"> 
        <img src="res/images/relampago.jpg" style="height : 190px; width: 655px; "/> 
    </div>
    <div class="mySlide"> 
        <img src="res/images/Disco-Dingo.jpg"style="height : 190px; width: 505px;  padding-left:150px;"/> 
    </div>
    <div class="mySlide"> 
        <img src="res/images/brtt.jpg"style="height : 190px; width: 505px;  padding-left:150px;"/> 
    </div>
    <div class="mySlide"> 
        <img src="res/images/yoda.jpg"style="height : 190px; width: 505px;  padding-left:150px;"/> 
    </div>        
  </div> -->

  <!-- Parte de slide das imagens automatizado -->
  <div class="auto-slider">
  <ul class="auto-slider__content">
    <li><img src="res/images/relampago.jpg" alt="relampago"></li>
    <li><img src="res/images/Disco-Dingo.jpg" alt="disco"></li>
    <li><img src="res/images/brtt.jpg" alt="brtt"></li>
    <li><img src="res/images/yoda.jpg" alt="yoda"></li>
  </ul>
</div> 
</div>

<!--
https://www.w3schools.com/howto/default.asp
-->
<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "158px";
  document.getElementById("navbar").style.marginLeft = "150px";
  document.getElementById("navbar").style.transition = "0.5s";

  //this changes how much the sidenav will navigate
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("navbar").style.marginLeft = "0px";
}
</script>   
</body>
</html> 