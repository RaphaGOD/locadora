<?php
$servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadora";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed!");
    }


    $sqlUser = mysqli_query($conn,"SELECT firstname, lastname, email, senha, DATE_FORMAT(datanasc,'%d/%m/%Y') as datanasc FROM usuario WHERE logado=1");
    $dadosUser = mysqli_fetch_row($sqlUser);
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

body {font-family: Arial, Helvetica, sans-serif; background-color: #6495ED}
form {border: 3px solid #ffffff;    
      /*isso muda o tamanho do form*/
      width: 400px;
      height: 610px;
      margin: auto;
      position: relative;}

input[type=text], input[type=password] {
  width: 100%;
  padding: 6px 10px;
  margin: 4px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
  width: 40%;
  border-radius: 25%;
}

.container {
  padding: 4px;
}


/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  .cancelbtn {
     width: 100%;
  }
}
</style>
</head>
<body>
<form action="editprofile.php" method="POST" style="background-size : cover;                                 
                                                    background-image: linear-gradient(#4169E1, #6A5ACD);">
  <div class="imgcontainer">
    <img src="res/images/user-icon1.png" alt="Avatar" class="avatar">
  </div>
<?php
  echo "<div class='container'>";
    echo "<label for='firstname'><b>Nome</b></label>";
    echo "<input type='text' placeholder='" . $dadosUser[0] . "' value='$dadosUser[0]' style='color : black' autocomplete='off' name='firstname' required>";

    echo "<label for='lastname'><b>Sobrenome</b></label>";
    echo "<input type='text' placeholder='" . $dadosUser[1] . "' value='$dadosUser[1]' style='color : black' autocomplete='off' name='lastname' required>";

    echo "<label for='datanasc'><b>Data de Nascimento</b></label>";
    echo "<input type='text' placeholder='" . $dadosUser[4] . "' autocomplete='off' name='datanasc' disabled>";

    echo "<label for='email'><b>Email</b></label>";
    echo "<input type='text' placeholder='" . $dadosUser[2] . "' value='$dadosUser[2]' name='email' autocomplete='off'>";

    echo "<label for='psw'><b>Password</b></label>";
    echo "<input type='password' placeholder='" . $dadosUser[3] . "' value='$dadosUser[3]' style='color : black' name='psw' required>";
        
    echo "<button type='submit'>Alterar</button>";
  echo "</div>";
?>
  <div class="container">
    <button type="button" class="cancelbtn" onclick="window.location.href='index.php'">&#9754;Voltar</button>
  </div>
</form>
</body>
</html>