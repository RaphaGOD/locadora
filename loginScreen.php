<?php
$servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadora";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed!");
    }
    
    $sqlusr = "SELECT * FROM usuario WHERE logado = '1'";
    $verify = mysqli_query($conn,$sqlusr);
    
    if(mysqli_num_rows($verify)>0)
    {
      echo "<html>";
      echo "<head></head>";
      echo "<body>";
      echo "<script type=\"text/javascript\">window.alert('Usuario ja logado');
      window.location.href = ' http://localhost/locadora/index.php';</script>";
      echo "</body>";
      echo "</html>";

    }

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif; background-color: #6495ED}
form {border: 3px solid #ffffff;    
      /*isso muda o tamanho do form*/
      width: 1000px;
      height: 550px;
      margin: auto;
      position: relative;}

input[type=text], input[type=password] {
  width: 98%;
  padding: 12px 20px;
  margin: 8px 0;
  margin-left : 10px;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

label{
  margin-left : 10px;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  margin-left : 10px;
  border: none;
  cursor: pointer;
  width: 98%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.registerbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #1b4edb;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
  width: 20%;
  border-radius: 25%;
}

.container {
  padding: 4px;
}

span.psw {
  float: right;
  padding-top: 16px;
  margin-right : 10px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
</head>
<body>
<form action="login.php" style="background-size : cover;                                 
                                background-image: linear-gradient(#4169E1, #6A5ACD);">
  <div class="imgcontainer">
    <img src="res/images/teste.jpg" alt="Avatar" class="avatar">
  </div>

  <div class="container">
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" autocomplete="off" name="email" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>
        
    <button type="submit">Login</button>
  </div>

  <div class="container">
    <button type="button" class="cancelbtn" onclick="window.location.href='index.php'">Cancel</button>
    <button type="button" class="registerbtn" onclick="window.location.href='signupScreen.php'">Register</button>
    <span class="psw">Forgot <a href="pswRecover.php">password?</a></span>
  </div>
</form>
</body>
</html>