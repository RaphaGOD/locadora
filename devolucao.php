<?php
$servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadora";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed!");
    }

    //Pegando o ID do user
    $sqllogado = "SELECT id FROM usuario WHERE logado='1'";
    $verificando = mysqli_query($conn,$sqllogado);
    //Essa variavel userId2 é desnecessaria, só trocar pelo userId e pegar o valor dele mesmo
    $userId2 = mysqli_fetch_row($verificando);
    $userId = $userId2[0];

    //sqls para pegar as placas
    if($sqlPlacaClio = mysqli_query($conn, "SELECT placa FROM carros WHERE nome='Clio'"))
        $placaClio2 = mysqli_fetch_row($sqlPlacaClio);
        $placaClio = $placaClio2[0];
    
    if($sqlPlacaCorolla = mysqli_query($conn, "SELECT placa FROM carros WHERE nome='Corolla'"))
        $placaCorolla2 = mysqli_fetch_row($sqlPlacaCorolla);
        $placaCorolla = $placaCorolla2[0];

    if($sqlPlacaCRV = mysqli_query($conn, "SELECT placa FROM carros WHERE nome='CR-V'"))
        $placaCRV2 = mysqli_fetch_row($sqlPlacaCRV);
        $placaCRV = $placaCRV2[0];


    //sqls para alterar o status do carro e deletar da tabela de aluguelCarros
    $sqlAttClio = "UPDATE carros SET alugado = '0' WHERE nome='Clio'";
    $sqlAttCorolla = "UPDATE carros SET alugado = '0' WHERE nome='Corolla'";
    $sqlAttCRV = "UPDATE carros SET alugado = '0' WHERE nome='CR-V'";
    $sqlDelClio = "DELETE FROM aluguelCarros WHERE placa ='$placaClio' and idCliente='$userId'";
    $sqlDelCorolla = "DELETE FROM aluguelCarros WHERE placa ='$placaCorolla' and idCliente='$userId'";
    $sqlDelCRV = "DELETE FROM aluguelCarros WHERE placa ='$placaCRV' and idCliente='$userId'";

    if ($_SERVER['REQUEST_METHOD'] === 'POST') 
    {
        if (isset($_POST['devolver_clio'])) 
        {
            $devolvendoClio = mysqli_query($conn, $sqlAttClio);
            $deletandoClio = mysqli_query($conn, $sqlDelClio);
            echo "<html>";
            echo "<head></head>";
            echo "<body>";
            echo "<script type=\"text/javascript\">window.alert('Carro de placa ". $placaClio ." devolvido com sucesso');
            window.location.href = ' http://localhost/locadora/index.php';</script>";
            echo "</body>";
            echo "</html>";
        } 
        elseif (isset($_POST['devolver_corolla'])) 
        {
            $devolvendoCorolla = mysqli_query($conn, $sqlAttCorolla);
            $deletandoCorolla = mysqli_query($conn, $sqlDelCorolla);
            echo "<html>";
            echo "<head></head>";
            echo "<body>";
            echo "<script type=\"text/javascript\">window.alert('Carro de placa ". $placaCorolla ." devolvido com sucesso');
            window.location.href = ' http://localhost/locadora/index.php';</script>";
            echo "</body>";
            echo "</html>";
        } 
        elseif (isset($_POST['devolver_CRV'])) 
        {
            $devolvendoCRV = mysqli_query($conn, $sqlAttCRV);
            $deletandoCRV = mysqli_query($conn, $sqlDelCRV);
            echo "<html>";
            echo "<head></head>";
            echo "<body>";
            echo "<script type=\"text/javascript\">window.alert('Carro de placa ". $placaCRV ." devolvido com sucesso');
            window.location.href = ' http://localhost/locadora/index.php';</script>";
            echo "</body>";
            echo "</html>";
        }
    }

mysqli_close($conn);

?>