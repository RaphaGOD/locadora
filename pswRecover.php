<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif; background-color: #6495ED}
form {border: 3px solid #ffffff;    
      /*isso muda o tamanho do form*/
      width: 1000px;
      height: 450px;
      margin: auto;
      padding-left : 10px;
      padding-right : 10px;
      position: relative;}

input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}
/* button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
} */

button:hover {
  opacity: 0.8;
}

.button {
  width: auto;
  padding: 10px 18px;
  background-color: #4CAF50;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.container {
  padding: 4px;
}

.cancelbtn, .button {
  float: left;
  color: white;
  margin: 8px 0;
  border: none;
  width: 50%;
}
span.psw {
  float: right;
  padding-top: 16px;
}
span.repsw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  span.repsw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
</head>
<body>
<form action="recover.php" style="background-size : cover;                                 
                                  background-image: linear-gradient(#4169E1, #6A5ACD);" method="POST">
  <div class="container">
    <h1>Password Recover</h1>
    <p>Please fill in this form to reset a password.</p>
    <hr>
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" autocomplete="off" name="email" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>
    
    <label for="repsw"><b>Re-enter Password</b></label>
    <input type="password" placeholder="Re-Enter Password" name="repsw" required>
    <div class="container">   
      <button type="button" class="cancelbtn" onclick="window.location.href='index.php'">Cancel</button>
      <button type="submit" class="button">Update</button>
    </div>
</div>
</form>
</body>
</html>