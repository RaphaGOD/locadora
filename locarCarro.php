   <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadora";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed!");
    }

    $sqlBanco = "SELECT * FROM carros WHERE alugado=0";
    $chamando = (mysqli_query($conn, $sqlBanco));
    ?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
    font-family: Arial, Helvetica, sans-serif; 
    background-color: #6495ED
}

form {
    border: 3px solid #ffffff;    
    /*isso muda o tamanho do form*/
    width: 1300px;
    height: 540px;
    margin: auto;
    margin-top : 20px;
    position: relative;
}

input[type=text]{
  width: 100%;
  padding: 12px 20px;
  margin: 4px;
  box-sizing: border-box;
  background-color : #EEE9E9; 
} 
.inputs{
  width: 90%;
  padding: 4px 10px;
  margin: 8px;
  margin-left : 6%;
  box-sizing: border-box;
  background-color : #CDC9C9;
}

.container{
    padding-left : 10px;
    padding-top : 5px;
    padding-right : 10px;
    margin-top : 3px;
    display : flex;
}

button:hover {
  opacity: 0.8;
}

.Alugar {
  width: auto;
  padding: 10px;
  margin-left : 40%;
  height : 40px;
  background-color: #4CAF50;
  border-style : inset;
  border-color: #bebebe;
}

.Clio{
    border-style : dotted;
    width : 33%;
    padding-right : 10px;
    margin-right : 5px;
    background-color : coral;
}

.Corolla{
    border-style : dotted;
    width : 33%;
    padding-right : 10px;
    margin-right : 5px;
    background-color : #00008D;
}

.CRV{
    border-style : dotted;
    width : 33%;
    padding-right : 10px;
    margin-right : 5px;
    background-color : #B22222;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  margin-left : 22px;
  margin-top : 10px;
  background-color: #f44336;
}

</style>
</head>
<body>
<form action="alugar.php" style="background-size : cover;                                 
                                 background-image: linear-gradient(#4169E1, #6A5ACD);" method="POST">
                            <!-- background-color:#474744; -->
                            <!-- background-position : center; -->
                            <!-- background-image: url('res/images/Disco-Dingo.jpg'); -->
<div class="container">
<?php
    
    $sqlClio = "SELECT nome,marca,tipo,ano,km,placa FROM carros WHERE alugado = 0 and nome='Clio' and marca='Renault'";
    $anuncioClio = mysqli_query($conn, $sqlClio); 
    $row = mysqli_fetch_row($anuncioClio);
    if(mysqli_num_rows($anuncioClio) > 0)
    {
        echo "<div class='Clio'>";
        echo "<img src='res/images/anuncioClio.png' style='height : 128px; width : 360px'/>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Nome do Carro : ". $row[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Marca do Carro : ". $row[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Tipo do Carro : ". $row[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Ano do Carro : ". $row[3] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Km do Carro : ". $row[4] ."' autocomplete='off' disabled
            style='color : #000000;'>";    
            echo "<input type='text' placeholder='Placa do Carro : ". $row[5] ."' autocomplete='off' disabled
            style='color : #000000;'>"; 
        echo "</div>";
        echo "<button type='submit' class='Alugar' name='alugar_clio'>Alugar</button>";
        echo "</div>";
    }
    

    $sqlCorolla = "SELECT nome,marca,tipo,ano,km,placa FROM carros WHERE alugado = 0 and nome='Corolla' and marca='Toyota'";
    $anuncioCorolla = mysqli_query($conn, $sqlCorolla); 
    $row = mysqli_fetch_row($anuncioCorolla);
    if(mysqli_num_rows($anuncioCorolla) > 0)
    {
        echo "<div class='Corolla'>";
        echo "<img src='res/images/anuncioCorolla.png' style='height : 128px; width : 360px'/>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Nome do Carro : ". $row[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Marca do Carro : ". $row[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Tipo do Carro : ". $row[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Ano do Carro : ". $row[3] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Km do Carro : ". $row[4] ."' autocomplete='off' disabled
            style='color : #000000;'>"; 
            echo "<input type='text' placeholder='Placa do Carro : ". $row[5] ."' autocomplete='off' disabled
            style='color : #000000;'>";    
        echo "</div>";
        echo "<button type='submit' class='Alugar' name='alugar_corolla'>Alugar</button>";
        echo "</div>";
    }

    $sqlCRV = "SELECT nome,marca,tipo,ano,km,placa FROM carros WHERE alugado = 0 and nome='CR-V' and marca='Honda'";
    $anuncioCRV = mysqli_query($conn, $sqlCRV); 
    $row = mysqli_fetch_row($anuncioCRV);
    if(mysqli_num_rows($anuncioCRV) > 0)
    {
        echo "<div class='CRV'>";
        echo "<img src='res/images/anuncioCRV.png' style='height : 128px; width : 360px'/>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Nome do Carro : ". $row[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Marca do Carro : ". $row[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Tipo do Carro : ". $row[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Ano do Carro : ". $row[3] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Km do Carro : ". $row[4] ."' autocomplete='off' disabled
            style='color : #000000;'>";  
            echo "<input type='text' placeholder='Placa do Carro : ". $row[5] ."' autocomplete='off' disabled
            style='color : #000000;'>";   
        echo "</div>";
        echo "<button type='submit' class='Alugar' name='alugar_CRV'>Alugar</button>";
        echo "</div>";
    }

    // if(mysqli_num_rows($anuncioCRV) > 0 && mysqli_num_rows($anuncioClio) > 0 && mysqli_num_rows($anuncioCorolla) == 0)
    // {
    //     echo "<div class='CRV' style='width : 50%; padding-right : 10px; margin-right : 5px'>";
    //     echo "<div class='Clio' style='width : 50%'>";
    // }
    // elseif(mysqli_num_rows($anuncioCRV) > 0 && mysqli_num_rows($anuncioCorolla) > 0 && mysqli_num_rows($anuncioClio) == 0)
    // {
    //     echo "<div class='CRV' style='width : 50%; padding-left : 30px; '>";
    //     echo "<div class='Corolla' style='width : 50%; padding-right : 10px; margin-right : 5px'>";
    // }
    // elseif(mysqli_num_rows($anuncioCorolla) > 0 && mysqli_num_rows($anuncioClio) > 0 && mysqli_num_rows($anuncioCRV) == 0)
    // {
    //     echo "<div class='Corolla' style='width : 50%'>";
    //     echo "<div class='Clio' style='width : 50%'>";
    // }
?>
</div>
</form>
<button type="button" class="cancelbtn" onclick="window.location.href='index.php'">&#9754;Voltar</button>
</body>
</html>