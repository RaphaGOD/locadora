-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 31, 2019 at 08:43 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `locadora`
--

-- --------------------------------------------------------

--
-- Table structure for table `aluguelCarros`
--

CREATE TABLE `aluguelCarros` (
  `idAluguel` int(11) NOT NULL,
  `dataRetirada` date NOT NULL,
  `dataDevolucao` date NOT NULL,
  `placa` char(8) NOT NULL,
  `idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `carros`
--

CREATE TABLE `carros` (
  `marca` varchar(10) NOT NULL,
  `nome` varchar(10) NOT NULL,
  `tipo` varchar(6) NOT NULL,
  `ano` int(4) NOT NULL,
  `km` int(6) NOT NULL,
  `alugado` tinyint(4) NOT NULL DEFAULT '0',
  `placa` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carros`
--

INSERT INTO `carros` (`marca`, `nome`, `tipo`, `ano`, `km`, `alugado`, `placa`) VALUES
('Toyota', 'Corolla', 'Sedan', 2018, 50000, 0, 'DHV-2002'),
('Honda', 'CR-V', 'SUV', 2015, 3000, 0, 'ERX-2779'),
('Renault', 'Clio', 'Hatch', 2019, 30000, 0, 'FKX-2779');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(12) NOT NULL COMMENT 'Senha entre 8 e 12',
  `datanasc` date NOT NULL,
  `logado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `firstname`, `lastname`, `email`, `senha`, `datanasc`, `logado`) VALUES
(1, 'Raphael', 'Cardoso', 'raphael@cardoso.com', '1234', '1996-02-27', 0),
(3, 'Raphael', 'Cardoso', 'raphael@petrere.com', '123', '2000-03-19', 0),
(4, 'Mark', 'Lee', 'marklee@NCT.com', '123', '1999-08-02', 1),
(5, 'Ricardo', 'Petrere', 'ricardopetrere@jpiaget.com', '2309', '1993-09-23', 0),
(6, 'Administrador', 'Site', 'admin@admin.com', 'admin', '1999-08-13', 0),
(7, 'Raphael', 'Cardoso', 'raphael@raphael.com', '123', '1980-03-13', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aluguelCarros`
--
ALTER TABLE `aluguelCarros`
  ADD PRIMARY KEY (`idAluguel`);

--
-- Indexes for table `carros`
--
ALTER TABLE `carros`
  ADD PRIMARY KEY (`placa`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aluguelCarros`
--
ALTER TABLE `aluguelCarros`
  MODIFY `idAluguel` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
