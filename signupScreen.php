<?php
$servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadora";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed!");
    }
    
    $sqlusr = "SELECT * FROM usuario WHERE logado = '1'";
    $verify = mysqli_query($conn,$sqlusr);
    
    if(mysqli_num_rows($verify)>0)
    {
      // header( 'Location: http://localhost/locadora/index.php' ) ;
      echo "<html>";
      echo "<head></head>";
      echo "<body>";
      echo "<script type=\"text/javascript\">window.alert('Usuario ja logado');
      window.location.href = ' http://localhost/locadora/index.php';</script>";
      echo "</body>";
      echo "</html>";

    }

?>

<!DOCTYPE html>
<html>
<style>
body {font-family: Arial, Helvetica, sans-serif; background-color: #6495ED}
* {box-sizing: border-box;}
form {
      border: 3px solid #ffffff;
      width: 800px;
      height: 520px;
      margin: auto;
      position: relative;
}

label[for="firstname"]{
  margin-right: 43px;
}

label[for="lastname"]{
  margin-right: 43px;
}


label[for="email"]{
  margin-right: 90px;
}

label[for="psw"]{
  margin-right: 12px;
}

input[type=date]{
  width: 30%;
  padding: 15px;
  margin: 5px 30px 22px 5px;
  display: inline-block;

}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 30%;
  padding: 15px;
  margin: 5px 30px 22px 5px;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

/* Add a background color when the inputs get focus */
input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for all buttons */
button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

button:hover {
  opacity:1;
}

/* Extra styles for the cancel button */
.cancelbtn {
  padding: 14px 20px;
  background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn, .signupbtn {
  float: left;
  width: 50%;
}

/* Add padding to container elements */
.container {
  padding: 16px;
}

/* Style the horizontal ruler */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Clear floats */
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}

/* Change styles for cancel button and signup button on extra small screens */
@media screen and (max-width: 300px) {
  .cancelbtn, .signupbtn {
     width: 100%;
  }
}
</style>
<body>

  <form class="modal-content" method="POST" action="signup.php" 
              style="background-size : cover;background-image: linear-gradient(#4169E1, #6A5ACD);">
              
    <div class="container">
      <h1>Sign Up</h1>
      <p>Please fill in this form to create an account.</p>
      <hr>
      <label for="firstname"><b>Nome</b></label>
      <input type="text" placeholder="Enter First Name" autocomplete="off" name="firstname" required>

      <label for="lastname"><b>Sobrenome</b></label>
      <input type="text" placeholder="Enter Last Name" autocomplete="off" name="lastname" required>
    <br>

      <label for="datanasc"><b>Aniversario</b></label>
      <input type="date" placeholder="Enter you Birthday" name="datanasc" required>

      <label for="email"><b>Email</b></label>
      <input type="text" placeholder="Enter Email" autocomplete="off" name="email" required>
    <br>
      <label for="psw"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="psw" required>

      <label for="psw-repeat"><b>Repeat Password</b></label>
      <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
    <br>
      
      <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

      <div class="clearfix">
        <button type="button"  class="cancelbtn" onclick="window.location.href='index.php'">Cancel</button>
        <button type="submit" class="signupbtn">Sign Up</button>
      </div>
    </div>
  </form>


<script>

</script> 

</body>
</html>
