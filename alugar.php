<?php

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadora";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed!");
    }

    //Pegando as placas de cada carro
    if($sqlPlacaClio = mysqli_query($conn, "SELECT placa FROM carros WHERE nome='Clio'"))
        $placaClio2 = mysqli_fetch_row($sqlPlacaClio);
        $placaClio = $placaClio2[0];

    if($sqlPlacaCorolla = mysqli_query($conn, "SELECT placa FROM carros WHERE nome='Corolla'"))
        $placaCorolla2 = mysqli_fetch_row($sqlPlacaCorolla);
        $placaCorolla = $placaCorolla2[0];

    if($sqlPlacaCRV = mysqli_query($conn, "SELECT placa FROM carros WHERE nome='CR-V'"))
        $placaCRV2 = mysqli_fetch_row($sqlPlacaCRV);
        $placaCRV = $placaCRV2[0];


    //Pegando o ID do user
    $sqllogado = "SELECT id FROM usuario WHERE logado='1'";
    $verificando = mysqli_query($conn,$sqllogado);
    $userId2 = mysqli_fetch_row($verificando);
    $userId = $userId2[0];


    //sql que altera o status de alugado e insere na tabela de aluguelCarros
    $sqlAttClio = "UPDATE carros SET alugado = '1' WHERE nome='Clio'";
    $sqlAttCorolla = "UPDATE carros SET alugado = '1' WHERE nome='Corolla'";
    $sqlAttCRV = "UPDATE carros SET alugado = '1' WHERE nome='CR-V'";
    $sqlAlugarClio = "INSERT INTO aluguelCarros (placa, dataRetirada, dataDevolucao, idCliente) values ('$placaClio', now(), now() + interval 7 day, $userId)";
    $sqlAlugarCorolla = "INSERT INTO aluguelCarros (placa, dataRetirada, dataDevolucao, idCliente) values ('$placaCorolla', now(), now() + interval 7 day, $userId)";
    $sqlAlugarCRV = "INSERT INTO aluguelCarros (placa, dataRetirada, dataDevolucao, idCliente) values ('$placaCRV', now(), now() + interval 7 day, $userId)";


    if(mysqli_num_rows($verificando) > 0)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {
            if (isset($_POST['alugar_clio'])) 
            {
                $atualizandoClio = mysqli_query($conn, $sqlAttClio);
                $alugandoClio = mysqli_query($conn, $sqlAlugarClio);
                echo "<html>";
                echo "<head></head>";
                echo "<body>";
                echo "<script type=\"text/javascript\">
                    window.alert('Carro alugado com sucesso, $placaClio'); 
                    window.location.href = ' http://localhost/locadora/index.php';
                </script>";
                echo "</body>";
                echo "</html>";
            } 
            elseif (isset($_POST['alugar_corolla'])) 
            {
                $atualizandoCorolla = mysqli_query($conn, $sqlAttCorolla);
                $alugandoCorolla = mysqli_query($conn, $sqlAlugarCorolla);
                echo "<html>";
                echo "<head></head>";
                echo "<body>";
                echo "<script type=\"text/javascript\">window.alert('Carro de placa ". $placaCorolla ." alugado com sucesso');
                window.location.href = ' http://localhost/locadora/index.php';</script>";
                echo "</body>";
                echo "</html>";
            } 
            elseif (isset($_POST['alugar_CRV'])) 
            {
                $atualizandoCRV = mysqli_query($conn, $sqlAttCRV);
                $alugandoCRV = mysqli_query($conn, $sqlAlugarCRV);
                echo "<html>";
                echo "<head></head>";
                echo "<body>";
                echo "<script type=\"text/javascript\">window.alert('Carro de placa ". $placaCRV ." alugado com sucesso');
                window.location.href = ' http://localhost/locadora/index.php';</script>";
                echo "</body>";
                echo "</html>";
            }
        }
    }
    else
    {
        echo "<html>";
        echo "<head></head>";
        echo "<body>";
        echo "<script type=\"text/javascript\">window.alert('Voce necessita estar logado para realizar locação');
        window.location.href = ' http://localhost/locadora/loginScreen.php';</script>";
        echo "</body>";
        echo "</html>";

    }

    mysqli_close($conn);        
?>