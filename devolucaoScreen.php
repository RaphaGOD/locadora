<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locadora";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed!");
    }

    $sqllogado = "SELECT id FROM usuario WHERE logado='1'";
    $verificando = mysqli_query($conn, $sqllogado);
    $userId2 = mysqli_fetch_row($verificando);
    $userId = $userId2[0];

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
    font-family: Arial, Helvetica, sans-serif; 
    background-color: #6495ED
}

form {
    border: 3px solid #ffffff;    
    /*isso muda o tamanho do form*/
    width: 1300px;
    height: 540px;
    margin: auto;
    margin-top : 20px;
    position: relative;
}

input[type=text]{
  width: 285px;
  padding: 6px 10px;
  margin: 2px;
  /* box-sizing: border-box; */
  background-color : #EEE9E9; 
} 
.inputs{
  padding: 4px 10px;
  margin: 8px;
  /* margin-left : 50px; */
  margin-right : 50px;
  width : 310px;
  /* padding-left : 10px; */
  /* box-sizing: border-box; */
  background-color : #CDC9C9;
}

.container{
    padding-left : 10px;
    padding-top : 5px;
    padding-right : 10px;
    margin-top : 3px;
    display : inline-block;
}

button:hover {
  opacity: 0.8;
}

.devolver {
  width: auto;
  padding: 10px;
  /* margin-left : 10%; */
  margin-right : 10px;
  margin-top : 20px;
  height : 80px;
  background-color: #4CAF50;
  border-style : inset;
  border-color: #bebebe;
}

.Clio{
    border-style : dotted;
    width : 97.5%;
    padding-right : 10px;
    margin-bottom : 5px;
    background-color : coral;
    display : flex;
}

.Corolla{
    border-style : dotted;
    width : 97.5%;
    padding-right : 10px;
    margin-bottom : 5px;
    background-color : #00008D;
    display : flex;
}

.CRV{
    border-style : dotted;
    width : 97.5%;
    padding-right : 10px;
    margin-bottom : 5px;
    background-color : #B22222;
    display : flex;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  margin-left : 22px;
  margin-top : 10px;
  background-color: #f44336;
}

</style>
</head>
<body>
<form action="devolucao.php" style="background-size : cover;                                 
                                 background-image: linear-gradient(#4169E1, #6A5ACD);" method="POST">
<div class="container">
<?php
    $sqlTbClio = "SELECT nome, ano, km FROM carros WHERE alugado=1 and placa='FKX-2779'";
    $anuncioTbClio = mysqli_query($conn, $sqlTbClio);
    $row2 = mysqli_fetch_row($anuncioTbClio);

    $sqlClio = "SELECT placa, DATE_FORMAT(dataRetirada,'%d/%m/%Y') as dataRetirada, DATE_FORMAT(dataDevolucao,'%d/%m/%Y') as dataDevolucao FROM aluguelCarros WHERE placa='FKX-2779' and idCliente='$userId'";
    $anuncioClio = mysqli_query($conn, $sqlClio); 
    $row = mysqli_fetch_row($anuncioClio);
    if(mysqli_num_rows($anuncioClio) > 0)
    {
        echo "<div class='Clio'>";
        echo "<img src='res/images/anuncioClio.png' style='height : 128px; width : 360px; margin-right : 50px;'/>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Nome do Carro : ". $row2[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Data de Retirada : ". $row[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Data de Devolucao estipulada : ". $row[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
        echo "</div>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Ano do Carro : ". $row2[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Km do Carro : ". $row2[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Placa do Carro : ". $row[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";    
        echo "</div>";
        echo "<button type='submit' class='devolver' name='devolver_clio'>Devolver</button>";
        echo "</div>";
    }
    
    $sqlTbCorolla = "SELECT nome, ano, km FROM carros WHERE alugado=1 and placa='DHV-2002'";
    $anuncioTbCorolla = mysqli_query($conn, $sqlTbCorolla);
    $row2 = mysqli_fetch_row($anuncioTbCorolla);

    $sqlCorolla = "SELECT placa, DATE_FORMAT(dataRetirada,'%d/%m/%Y') as dataRetirada, DATE_FORMAT(dataDevolucao,'%d/%m/%Y') as dataDevolucao FROM aluguelCarros WHERE placa='DHV-2002' and idCliente='$userId'";
    $anuncioCorolla = mysqli_query($conn, $sqlCorolla); 
    $row = mysqli_fetch_row($anuncioCorolla);
    if(mysqli_num_rows($anuncioCorolla) > 0)
    {
        echo "<div class='Corolla'>";
        echo "<img src='res/images/anuncioCorolla.png' style='height : 128px; width : 360px; margin-right : 50px;'/>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Nome do Carro : ". $row2[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Data de Retirada : ". $row[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Data de Devolucao estipulada : ". $row[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
        echo "</div>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Ano do Carro : ". $row2[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Km do Carro : ". $row2[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Placa do Carro : ". $row[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";    
        echo "</div>"; 
        echo "<button type='submit' class='devolver' name='devolver_corolla'>Devolver</button>";
        echo "</div>";
    }

    $sqlCRV = "SELECT placa, DATE_FORMAT(dataRetirada,'%d/%m/%Y') as dataRetirada, DATE_FORMAT(dataDevolucao,'%d/%m/%Y') as dataDevolucao FROM aluguelCarros WHERE placa='ERX-2779' and idCliente='$userId'";
    $sqlTbCRV = "SELECT nome, ano, km FROM carros WHERE alugado=1 and placa='ERX-2779'";
    $anuncioCRV = mysqli_query($conn, $sqlCRV); 
    $anuncioTbCRV = mysqli_query($conn, $sqlTbCRV); 
    $row = mysqli_fetch_row($anuncioCRV);
    $row2 = mysqli_fetch_row($anuncioTbCRV);
    if(mysqli_num_rows($anuncioCRV) > 0)
    {
        echo "<div class='CRV'>";
        echo "<img src='res/images/anuncioCRV.png' style='height : 128px; width : 360px; margin-right : 50px;'/>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Nome do Carro : ". $row2[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Data de Retirada : ". $row[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Data de Devolucao estipulada : ". $row[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
        echo "</div>";
        echo "<div class = 'inputs'>";
            echo "<input type='text' placeholder='Ano do Carro : ". $row2[1] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Km do Carro : ". $row2[2] ."' autocomplete='off' disabled
            style='color : #000000;'>";
            echo "<input type='text' placeholder='Placa do Carro : ". $row[0] ."' autocomplete='off' disabled
            style='color : #000000;'>";    
        echo "</div>";
        echo "<button type='submit' class='devolver' name='devolver_CRV'>Devolver</button>";
        echo "</div>";
    }
    mysqli_close($conn);        
?>
</div>
</form>
<button type="button" class="cancelbtn" onclick="window.location.href='index.php'">&#9754;Voltar</button>
</body>
</html>